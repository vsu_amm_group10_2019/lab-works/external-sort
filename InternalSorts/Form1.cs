﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InternalSorts
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Utils.GenerateRandomFile("Start", 100);
            textBox1.Text = Utils.PrintFile("Start");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SinglePhaseSimpleSort sort = new SinglePhaseSimpleSort();
            sort.SortFile("Start");
            textBox2.Text = Utils.PrintFile("Start");
        }
    }
}
