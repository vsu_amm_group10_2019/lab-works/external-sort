﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace InternalSorts
{
     public class NaturalSequence : IDisposable
    {
        private BinaryReader Reader { get; set; }
        private BinaryWriter Writer { get; set; }
        public FileInfo FileInfo { get; }
        public int Element { get; private set; }        
        public bool IsEndOfSequence { get; private set; }

        public bool HasElement { get; private set; }

        public NaturalSequence(string filePath)
        {
            FileInfo = new FileInfo(filePath);
            IsEndOfSequence = true;
            HasElement = false;

        }
        public void ResetSequence()
        {
            IsEndOfSequence = IsFileProcessed();
        }
        public void StartRead()
        {
            Reader = new BinaryReader(File.Open(FileInfo.FullName, FileMode.Open));
            IsEndOfSequence = IsEndOfFile();
        }
        public void StopRead()
        {
            Reader.Close();
        }
        public void StartWrite()
        {
            Writer = new BinaryWriter(File.Open(FileInfo.FullName, FileMode.Create));
        }
        public void StopWrite()
        {
            Writer.Close();
        }

        private void WriteElem(int element)
        {
            Writer.Write(element);
        }
        private bool IsEndOfFile()
        {
            return Reader.BaseStream.Position == Reader.BaseStream.Length;
        }

        public bool IsFileProcessed() // Файл обработан, если он полностью прочитан и все его элементы обработаны
        {
            return IsEndOfFile() && !HasElement;
        }
        public void Dispose()
        {
            Reader?.Dispose();
            Writer?.Dispose();
        }
        public void ReadElem()
        {
            if (!IsEndOfFile())
            {
                Element = Reader.ReadInt32();
                HasElement = true; // Есть элемент для обработки
            }
            else
            {
                HasElement = false; // Нет элементов для обработки
            }
        }
        public void CopyElementTo(NaturalSequence sequence)
        {
            sequence.WriteElem(Element);
            HasElement = false;
            sequence.Element = Element;
            if (!IsEndOfFile())
            {
                ReadElem();
            }
            IsEndOfSequence = (IsEndOfFile() && !HasElement) || sequence.Element > Element; //Последовательность закончена,
                                                                                            //если достигнут конец файла и нет элементов для обработки
                                                                                            //или же следующий элемент не упорядочен 
        }
        public void CopySequence(NaturalSequence sequence) //Копирование упорядоченной последовательности
        {
            while (!IsEndOfSequence)
            {
                CopyElementTo(sequence);
            }
           
        }
        
    }
}
