﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InternalSorts
{
    public class Sequence : IDisposable
    {
        private BinaryReader Reader { get; set; }
        private BinaryWriter Writer { get; set; }
        public FileInfo FileInfo { get; }
        public int Element { get; private set; }
        public bool ReadyToCopy { get; private set; }
        private int Remaning { get; set; }
        public Sequence (string filePath)
        {
            FileInfo = new FileInfo(filePath);
        }
        public void NewRun (int length)
        {
            Remaning = length;
        }
        public void StartRead()
        {
            Reader = new BinaryReader(File.Open(FileInfo.FullName, FileMode.Open));
        }
        public void StopRead()
        {
            Reader.Close();
        }
        public void StartWrite()
        {
            Writer = new BinaryWriter(File.Open(FileInfo.FullName, FileMode.Create));
        }
        public void StopWrite()
        {
            Writer.Close();
        }
        public void WriteElem(int element)
        {
            Writer.Write(element);
        }
        public bool IsEndOfFile()
        {
            return Reader.BaseStream.Position == Reader.BaseStream.Length;
        }
        public bool IsEndOfSeries()
        {
            return IsEndOfFile() || Remaning == 0;
        }
        public void Dispose()
        {
            Reader?.Dispose();
            Writer?.Dispose();
        }
        public void ReadElem()
        {
            if (!IsEndOfSeries())
            {
                Element = Reader.ReadInt32();
                ReadyToCopy = true;
            } 
        }
        public void CopyElementTo(Sequence sequence)
        {
            if (ReadyToCopy)
            {
                sequence.WriteElem(Element);
                Remaning--;
                ReadyToCopy = false;
            }
        }
        public void CopyAllTo(Sequence sequence)
        {
            ReadElem();
            do
            {
                CopyElementTo(sequence);
                ReadElem();
            } while (ReadyToCopy);
        }
    }
}
