﻿using System;

namespace InternalSorts
{
    public class SinglePhaseSimpleSort
    {
        //Количество вспомогательных файлов / 2
        const int FilesCount = 4;
        //Проход с первой группы файлов
        private bool _isFirst = true;
        //Длина начальной серии - 1
        private int _length = 1;

        // Последняя серия, в которую записывались элементы 
        private Sequence _result;

        public void SortFile(string fileName)
        {
            Sequence sequence = new Sequence(fileName);
            Sequence[] firstSequences = InitSequences(FilesCount);
            Sequence[] secondSequences = InitSequences(FilesCount);
                        
            FirstMerge(sequence, firstSequences); // Изначальное разпределение данных по половине вспомогательных файлов
            while (Merge(firstSequences, secondSequences) > 1)
            {
                _length *= FilesCount;
                _isFirst = !_isFirst;
            }
            //Переносим результат в исходный файл
            sequence.StartWrite();
            _result.StartRead();
            _result.NewRun(_length * FilesCount);
            _result.CopyAllTo(sequence);
            _result.StopRead();
            sequence.StopWrite();
            foreach(Sequence tmp in firstSequences)
            {
                tmp.FileInfo.Delete();
                tmp.Dispose();
            }
            foreach(Sequence tmp in secondSequences)
            {
                tmp.FileInfo.Delete();
                tmp.Dispose();
            }
            sequence.Dispose();
        }

        private void FirstMerge(Sequence sequence, Sequence[] sequences)
        {
            sequence.StartRead();
            
            foreach (Sequence tmp in sequences)
            {
                tmp.StartWrite();
            }
            sequence.NewRun(_length);
            int destination = 0;
            int count = 0;
            while (!sequence.IsEndOfFile())
            {
                destination %= sequences.Length;
                sequence.ReadElem();
                sequence.CopyElementTo(sequences[destination]);
                destination++;
                count++;
                if (count == _length)
                {
                    sequence.NewRun(_length);
                    count = 0;
                }
            }
            sequence.StopRead();
            foreach (Sequence tmp in sequences)
            {
                tmp.StopWrite();
            }
        }
        
        private int Merge(Sequence[] firstSequences, Sequence[] secondSequences)
        {
            int countOfIterations = 0;

            Sequence[] sourceSequences;
            Sequence[] destinationSequences;
            //Так как в однофазной сортировке у нас есть только Merge, то наши наборы файлов поочередно будут как входными, так и выходными
            if (_isFirst)
            {
                sourceSequences = firstSequences;
                destinationSequences = secondSequences;
            }
            else
            {
                sourceSequences = secondSequences;
                destinationSequences = firstSequences;
            }
            
            foreach (Sequence source in sourceSequences)
            {
                source.StartRead();
                source.NewRun(_length);
                source.ReadElem();
            }
            foreach (Sequence destination in destinationSequences)
            {
                destination.StartWrite();
            }

            int dest = 0;
            int count = 0;
            while (FirstReady(sourceSequences) > -1)
            {
                while (FirstReady(sourceSequences) > -1)
                {
                    // Делаем привычный Merge серий в файл
                    dest %= destinationSequences.Length;
                    int i = GetIndexOfMin(sourceSequences);
                    sourceSequences[i].CopyElementTo(destinationSequences[dest]);
                    sourceSequences[i].ReadElem();
                    count++;
                    _result = destinationSequences[dest];
                    // Как только мы собрали в файле серию длиной _length * кол-во входных файлов (размер серии на следующем шаге)
                    // то меняем целевой файл
                    if (count % (sourceSequences.Length * _length) == 0)
                    {
                        dest++;
                    }
                }
                countOfIterations++;
                foreach (Sequence tmp in sourceSequences)
                {
                    tmp.NewRun(_length);
                    tmp.ReadElem();
                }
            }
            
            foreach (Sequence source in sourceSequences)
            {
                source.StopRead();
            }
            foreach (Sequence destination in destinationSequences)
            {
                destination.StopWrite();
            }
            
            return countOfIterations;
        }
        
        private int FirstReady(Sequence[] sequences)
        {
            int first = 0;
            while (first < sequences.Length && sequences[first].ReadyToCopy==false)
            {
                first++;
            }
            if (first == sequences.Length)
            {
                return -1;
            }
            return first;
        }
        private int GetIndexOfMin(Sequence[] sequences)
        {
            int max = FirstReady(sequences);
            if (max == -1)
            {
                throw new Exception("Пусто");
            }
            for (int i = max + 1; i < sequences.Length; i++)
            {
                if (sequences[i].ReadyToCopy && sequences[i].Element < sequences[max].Element)
                {
                    max = i;
                }
            }
            return max;
        }
        
        private Sequence[] InitSequences(int count)
        {
            Sequence[] sequences = new Sequence[count];
            for (int i = 0; i < count; i++)
            {
                sequences[i] = new Sequence("tmp" + Guid.NewGuid().ToString());
            }
            return sequences;
        }
    }
}