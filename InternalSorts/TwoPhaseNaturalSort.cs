﻿using System;
using System.Linq;

namespace InternalSorts
{
    public class TwoPhaseNaturalSort //Многопутивая двухфазная естественное слабалнсированное слиянием
    {
        const int FilesCount = 4;
        private bool _done;

        private void Merge(NaturalSequence sequence, params NaturalSequence[] sequences)
        {
            _done = false;
            sequence.StartWrite();
            foreach (NaturalSequence tmp in sequences)
            {
                tmp.StartRead();
                tmp.ReadElem();
            }

            _done = NumberOfSequencesWithData(sequences) == 1;
            
            while (FirstReady(sequences) > -1)
            {
                while (FirstReady(sequences) > -1)
                {
                    int index = GetIndexOfMin(sequences);
                    sequences[index].CopyElementTo(sequence);
                }
                
            }

            sequence.StopWrite();
            foreach (NaturalSequence tmp in sequences)
            {
                tmp.StopRead();
            }
        }

        private void Distribute(NaturalSequence sequence, params NaturalSequence[] sequences)
        {
            sequence.StartRead();
            sequence.ReadElem();
            foreach (NaturalSequence tmp in sequences)
            {
                tmp.StartWrite();
            }

            foreach (NaturalSequence tmp in sequences)
            {
                if (!sequence.IsFileProcessed())
                {
                    sequence.CopySequence(tmp);
                    sequence.ResetSequence();
                }
            }

            int destination = 0;

            while (!sequence.IsFileProcessed())
            {
                NaturalSequence destSequence = sequences[destination];
                if (sequence.Element > destSequence.Element) //Если следующая серия является продолжением той, которая уже есть в файле, то объединяем их
                {
                    sequence.CopySequence(destSequence);
                    sequence.ResetSequence();
                }

                sequence.CopySequence(destSequence);
                sequence.ResetSequence();
                destination = destination == sequences.Length - 1 ? 0 : destination + 1;
            }

            foreach (NaturalSequence tmp in sequences)
            {
                tmp.StopWrite();
            }

            sequence.StopRead();
        }

        private NaturalSequence[] InitSequences(int count)
        {
            NaturalSequence[] sequences = new NaturalSequence[count];
            for (int i = 0; i < count; i++)
            {
                sequences[i] = new NaturalSequence("tmp" + i.ToString());
            }

            return sequences;
        }

        private int FirstReady(NaturalSequence[] sequences)
        {
            int first = 0;
            while (first < sequences.Length && sequences[first].IsFileProcessed())
            {
                first++;
            }

            if (first == sequences.Length)
            {
                return -1;
            }

            return first;
        }

        private int GetIndexOfMin(NaturalSequence[] sequences)
        {
            int max = FirstReady(sequences);
            if (max == -1)
            {
                throw new Exception("Пусто");
            }

            for (int i = max + 1; i < sequences.Length; i++)
            {
                if (!sequences[i].IsFileProcessed() && sequences[i].Element < sequences[max].Element)
                {
                    max = i;
                }
            }

            return max;
        }

        private int NumberOfSequencesWithData(NaturalSequence[] sequences)
        {
            return sequences.Count(sequence => sequence.HasElement);
        }
        
        public void SortFile(string fileName)
        {
            NaturalSequence sequence = new NaturalSequence(fileName);
            NaturalSequence[] sequences = InitSequences(FilesCount);
            do
            {
                Distribute(sequence, sequences);
                Merge(sequence, sequences);
            } while (!_done);

            foreach (NaturalSequence tmp in sequences)
            {
                tmp.FileInfo.Delete();
                tmp.Dispose();
            }

            sequence.Dispose();
        }
    }
}